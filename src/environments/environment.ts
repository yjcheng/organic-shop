// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDZUtura2dpAMkddnZHuvDjMi7hpsGGIxI',
    authDomain: 'organic-shop-dcf62.firebaseapp.com',
    databaseURL: 'https://organic-shop-dcf62.firebaseio.com',
    projectId: 'organic-shop-dcf62',
    storageBucket: 'organic-shop-dcf62.appspot.com',
    messagingSenderId: '400209229935',
    appId: '1:400209229935:web:4b29f112e0558ba039b59a',
    measurementId: 'G-54YPKZPT0D'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
